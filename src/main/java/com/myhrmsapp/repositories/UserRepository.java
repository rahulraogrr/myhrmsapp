package com.myhrmsapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.myhrmsapp.models.User;

@Repository("UserRepository")
public interface UserRepository extends JpaRepository<User, Long> {

}
