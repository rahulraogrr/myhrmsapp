package com.myhrmsapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.myhrmsapp.models.User;

@SpringBootApplication
public class MyhrmsappApplication {
	public static void main(String[] args) {
		SpringApplication.run(MyhrmsappApplication.class, args);
	}
}
