package com.myhrmsapp.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity(name = "user_master")
@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class User{
	@Id @GeneratedValue
	private Long id;
	private String username;
	private String password;
}