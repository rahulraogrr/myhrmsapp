# myhrmsapp
MyHrmsApp 

## Badges

- code coverage percentage: ![coverage](https://img.shields.io/badge/coverage-80%25-yellowgreen)
- stable release version: ![version](https://img.shields.io/badge/version-0.0.1-blue)
- static code analysis grade: <a href="https://www.codacy.com/manual/rahulraogrr/myhrmsapp?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=rahulraogrr/myhrmsapp&amp;utm_campaign=Badge_Grade"><img src="https://api.codacy.com/project/badge/Grade/50a2abb0c611439b80c16a0b2987a951"/></a>
- [SemVer](https://semver.org/) version observance: ![semver](https://img.shields.io/badge/semver-2.0.0-blue)

# Modules

| Modules                       | Version          | Supported          |
| --------------------------    | --------------   | ------------------ |
| HEROKU Support                | 0.0.1-SNAPSHOT   | :white_check_mark: |
| Time and attendance           | 0.0.1-SNAPSHOT   | :x:                |
| Administrator Module          | 0.0.1-SNAPSHOT   | :x:                |
| Leave Management Module       | 0.0.1-SNAPSHOT   | :x:                |
| Employee On-boarding Module   | 0.0.1-SNAPSHOT   | :x:                |
| Performance Management Module | 0.0.1-SNAPSHOT   | :x:                |
| HR Analytics/Reporting        | 0.0.1-SNAPSHOT   | :x:                |
| Recruitment Module            | 0.0.1-SNAPSHOT   | :x:                |

# Technical Specification

| Software           | Version        |
| ------------------ | -------------- |
| Java               | 1.8            |
| Spring Boot        | 2.1.7.RELEASE  |
| Tomcat             | 9.0.22         |

# Authors

* **Rahul Rao Gonda** - *Initial work* - [rahulraogrr](https://github.com/rahulraogrr)